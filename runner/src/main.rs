use wasmer_runtime::validate;
use wasmer_runtime::compile;
pub use wasmer_runtime::Value;
pub use wasmer_runtime::Func;
use wasmer_runtime::imports;
pub use wasmer_runtime::Instance;
use wasmer_runtime::error;
use wasmer_runtime::WasmPtr;
use wasmer_runtime::Array;
use wasmer_runtime::Memory;

type Alloc<'a> = Func<'a, u32, WasmPtr<u8, Array>>;

const LENGTH: u32 = 4;

struct Allocator<'a> {
	alloc: Alloc<'a>,
	memory: &'a Memory,
}

impl<'a> Allocator<'a> {
	fn new(instance: &'a Instance) -> Self {
		let alloc: Alloc = instance.func("get_memory").unwrap();
		let ctx = instance.context();
		let memory = ctx.memory(0);
		Allocator {
			alloc,
			memory,
		}
	}
	fn insert_vec(&self, vec: Vec<u8>) -> u32 {
		let length = vec.len() as u32;
		let ptr = self.alloc.call(length + 4).unwrap();
		let mut len = length.to_ne_bytes().to_vec();
		let mut i = 0;
		for v in ptr.deref(self.memory, 0, 4).unwrap() {
			v.set(len[i]);
			i += 1;
		}
		let mut i = 0;
		for v in ptr.deref(self.memory, 4, length).unwrap() {
			v.set(vec[i]);
			i += 1;
		}
		ptr.offset()
	}
	fn extract_vec(&self, ptr: WasmPtr<u8, Array>) -> Vec<u8> {
		let q = ptr.deref(self.memory, 0, 8).unwrap().to_vec();
		let len = utils::strip_cell(ptr.deref(self.memory, 0, 4).unwrap().to_vec());
		let mut buf = [0u8; 4];
		buf.copy_from_slice(&len);
		let length: u32 = unsafe { std::mem::transmute(buf) };
		let res = utils::strip_cell(ptr.deref(self.memory, 4, length).unwrap().to_vec());
		res
	}
}


fn load_instance<'a>(path: &'a str) {
	let source = read_source(path);
	let import_object = imports! {};
	dbg!(validate(&source));
	let module = compile(&source).unwrap();
	// dbg!(module.info());
	let instance = module.instantiate(&import_object).unwrap();

	// let allocator = Allocator::new(&instance);
	let sum_vec: Func<u32, WasmPtr<u8, Array>> = instance.func("sum_vec").unwrap();

	let ctx = instance.context();
	let alloc: Func<u32, WasmPtr<u8, Array>> = instance.func("get_memory").unwrap();

	for _ in 0..4 {
		let ptr = alloc.call(LENGTH).unwrap();
		let memory = ctx.memory(0);
		// let mut i = 0;
		// for v in ptr.deref(memory, 0, LENGTH).unwrap() {
		// 	v.set(i);
		// 	i += 1;
		// }

		let ptr = sum_vec.call(ptr.offset()).unwrap();

		let mut buf = [0u8; 4];
		let len = utils::strip_cell(ptr.deref(memory, 0, 4).unwrap().to_vec());
		buf.copy_from_slice(&len);
		let length: u32 = unsafe { std::mem::transmute(buf) };
		dbg!(ptr.deref(memory, 4, length));
		let x = utils::strip_cell(ptr.deref(memory, 4, length).unwrap().to_vec());

		// let ptr = allocator.insert_vec(vec![1,2,3,4]);
		// let ptr = sum_vec.call(ptr).unwrap();
		// let x = allocator.extract_vec(ptr);

		dbg!(x);

	}


	let add_one: Func<i32, i32> = instance.func("add_one").unwrap();

	for i in 0..1 {
		let value = add_one.call(i).unwrap();
		dbg!(value);
	}
}


fn read_source<'a>(path: &'a str) -> Vec<u8> {
	use std::path::PathBuf;
	use std::fs::File;
	use std::io::prelude::*;
	let path = PathBuf::from(path);
	let mut file = File::open(path).unwrap();
	let mut contents = Vec::new();
	file.read_to_end(&mut contents).unwrap();
	contents
}


fn main() {
	load_instance("../target/wasm32-unknown-unknown/release/test01.wasm");
	println!("Hello, world!");
}



#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	fn name() {
		load_instance("../target/wasm32-unknown-unknown/release/test01.wasm");
		assert_eq!(1, 2);
	}
}
