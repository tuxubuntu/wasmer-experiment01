
const SIZE: usize = std::mem::size_of::<u32>();

pub fn unpack(vec_ptr: u32) -> Vec<u8> {
	let mut buf = [0u8; SIZE];
	let len: Vec<u8> = unsafe {
		Vec::from_raw_parts(vec_ptr as *mut u8, SIZE, SIZE)
	};
	buf.copy_from_slice(&len);
	let len: u32 = unsafe { std::mem::transmute(buf) };
	let len = len as usize;
	unsafe { Vec::from_raw_parts(vec_ptr as *mut u8, len, len) }
}

pub fn pack(mut data: Vec<u8>) -> u32 {
	let mut len = data.len().to_ne_bytes().to_vec();
	len.append(&mut data);
	let ptr = len.as_ptr();
	std::mem::forget(len);
	ptr as _
}

pub fn strip_cell(v: Vec<std::cell::Cell<u8>>) -> Vec<u8> {
	let mut res = v.clone();
	let ptr;
	let len;
	let cap;
	{
		ptr = res.as_mut_ptr();
		cap = res.capacity();
		len = res.len();
	}
	std::mem::forget(res);
	// https://rust.godbolt.org/z/zTdAg_
	unsafe { Vec::from_raw_parts(ptr.cast(), len, cap) }
}

