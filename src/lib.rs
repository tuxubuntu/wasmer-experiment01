
#[no_mangle]
pub fn get_memory(size: u32) -> u32 {
	let vec = std::iter::repeat(0).take(size as usize).collect::<Vec<u8>>();
	let ptr = vec.as_ptr();
	std::mem::forget(vec);
	ptr as _
}

#[no_mangle]
pub fn sum_vec(vec_ptr: u32) -> u32 {
	let vec = utils::unpack(vec_ptr);

	// let mut sum = 0;
	// for e in vec.iter() {
	// 	sum += e;
	// }

	utils::pack(vec![42])
}

#[no_mangle]
pub fn sum_vec2(vec_ptr: u32, vec_len: u32) -> u32 {
	let vec = utils::unpack(vec_ptr);

	// let mut sum = 0;
	// for e in vec.iter() {
	// 	sum += e;
	// }

	utils::pack(vec![42])
}


#[no_mangle]
pub fn add_one(x: i32) -> i32 {
	x + 1
}
